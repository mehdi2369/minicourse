# Basic exercises: QPC with magnetic field

In this exercise we will start from the QPC example from the lecture and add magnetic field to it.

### Exercise A: Execute the example from the lecture
The following cells contain a slightly reorganized but otherwise verbatim copy of the QPC example from the lecture.  The computation itself has not been changed at all, but the code has been organized into functions for clarity and easier reuse.

Execute the following five cells to recover the outputs that you have already seen in the lecture.

Try to understand what is being done.  If you would like to know more about a particular feature of Kwant, do not hesitate to consult the [documentation of Kwant](https://kwant-project.org/doc/1/)

```python
from math import exp
from matplotlib import pyplot
import numpy as np
import kwant
```

```python
#### Define model constants.

a = 0.5                     # lattice constant
m = 1.23                    # effective mass

# Create a square lattice with specified lattice constant.
# Specify that it has a single orbital per site.
sq = kwant.lattice.square(a=a, norbs=1)
```

```python
#### Define model.

# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite                     # Value function!
model[sq.neighbors()] = -1 / (2 * m * a**2)  # Constant value!

# We could have also assigned the hoppings individually:
# > model[sq(0, 0), sq(1, 0)] = model[sq(0, 0), sq(0, 1)] = ...
#
# Or a list of hoppings:
# > model[[(sq(0, 0), sq(1, 0)), (sq(0, 0), sq(0, 1))]] = ...

kwant.plot(model);
```

```python
#### Define a function that builds a rectangular system with two leads.

def make_system(L, W, model):
    # Define shape of central region: a rectangle of length L and width W, centered
    # around the origin.
    def rectangle(site):
        x, y = site.pos
        return -L <= 2*x <= L and -W <= 2*y <= W

    # Fill central region with model.
    syst = kwant.Builder()
    syst.fill(model, rectangle, sq(0, 0))

    # Fill lead with same model, but with substituted parameter so that the
    # gate voltage can be set to zero for the leads.
    lead = kwant.Builder(kwant.TranslationalSymmetry(sq.vec((-1, 0))))
    lead.fill(model.substituted(voltage='voltage_lead'),
              lambda site: -W <= 2*site.pos[1] <= W,
              sq(0, 0))
    syst.attach_lead(lead)
    syst.attach_lead(lead.reversed())

    # Finalize the system and plot it, coloring the sites according to their potential
    # (=their onsite hamiltonian value).
    return syst.finalized()
```

```python
#### Build the system and plot it.

# Use the above function to build the system from our model.
fsyst = make_system(14, 9, model)

# Define model parameters inside a dictionary.
params = dict(voltage_lead=0, voltage=-4, width=3, thickness=4)

# Plot the system.  Color the sites according to their potential.
kwant.plot(fsyst, site_color=lambda i: fsyst.hamiltonian(i, i, params=params));
```

```python
# The potential can be seen clearer when we use a different plotting function.
kwant.plotter.map(fsyst, lambda i: fsyst.hamiltonian(i, i, params=params));
```

```python
# Plot bands and choose energy such that a few modes are open.
kwant.plotter.bands(fsyst.leads[0], params=params)
energy = -5
```

```python
# Define a function to plot conductance as a function of a changing parameter.
def plot_conductance(fsyst, energy, param_name, param_values, other_params):
    params = other_params.copy()

    gs = [kwant.smatrix(fsyst, energy, params=params).transmission(1, 0)
          for params[param_name] in param_values]
    pyplot.plot(param_values, gs, 'k-')
    pyplot.xlabel(param_name)
    pyplot.ylabel("$g \quad [e^2/h]$");
```

```python
# Use above function to show conductance quantization.
plot_conductance(fsyst, energy, 'voltage', np.linspace(0, -4, 50), params)
```

### Exercise B: Compute the current density and plot it
Kwant contains a `kwant.operator` submodule that allows to compute generic density and current operators.  We do not have time to cover it in depth here, but we will demonstrate basic usage, so that you can get an idea of how it works.  Plotting currents will also come handy once we have added magnetic field!

If you would like to know more, you may consult the [operator tutorial](https://kwant-project.org/doc/1/tutorial/operators) and the [reference documentation of the operator submodule](https://kwant-project.org/doc/1/reference/kwant.operator).

The following code cell defines a function that computes and plots the current density in the system.  Familiarize yourself with the contents of the cell, and execute it.  You will see that in the absence of magnetic field electric current passes through the entire cross section of the QPC constriction.

```python
def plot_current_density(fsyst, energy, params):
    # Compute the scattering wave function due to the modes in lead 0 at given energy.
    psi = kwant.wave_function(fsyst, energy, params=params)(0)
    # Create a current operator.
    J = kwant.operator.Current(fsyst).bind(params=params)
    # Compute the current observable.
    current = sum(J(p) for p in psi)
    # Plot the current as a stream plot.
    kwant.plotter.current(fsyst, current)
```

```python
plot_current_density(fsyst, energy, params)
```

### Exercise C: Set horizontal and vertical hoppings separately
In the definition of `model` above, both horizontal and vertical nearest neighbor hoppings are set in the following line:
```
model[sq.neighbors()] = -1 / (2 * m * a**2)
```

However, in order to add magnetic field, we will want to use different values for vertical and horizontal hoppings.

The following cell repeats the code that creates the model.  The line that sets the hoppings is placed in-between markers (`########`).

Replace this line by two lines that separately set the horizontal and vertical hoppings to the same value as currently.

Hint: In Kwant builders onsite Hamiltonians are indexed by single sites (for example `model[sq(0, 0)] = onsite`), and hoppings by 2-tuples of sites.  Look, for example, how hoppings were defined in the section 6 of the lecture.  Or consult the [first Kwant tutorial](https://kwant-project.org/doc/1/tutorial/first_steps).

Execute the cells.  There should be no difference in the output, because the model did not change.

```python
# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite

########
model[sq.neighbors()] = -1 / (2 * m * a**2)
########
```

```python
#### Solution of exercise C

# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite

########
model[sq(0, 0), sq(1, 0)] = -1 / (2 * m * a**2)
model[sq(0, 0), sq(0, 1)] = -1 / (2 * m * a**2)
########
```

```python
#### Verify that the result did not change

fsyst = make_system(14, 9, model)
plot_conductance(fsyst, energy, 'voltage', np.linspace(0, -4, 50), params)
```

### Exercise D: Use a value function for the horizontal hoppings
In order to add magnetic field to the model, we want to use [Peierls substitution](https://topocondmat.org/w2_majorana/Peierls.html).  Since our leads are horizontal, we need to choose a gauge that is compatible with the horizontal translational symmetry of the leads.  We will choose a Landau gauge such that the phase of the **horizontal** hoppings will depend on their **vertical** position.

As the next step in this direction, add a value function `hopx` that does not perform any computation but simply returns the current constant value of the hoppings.  Use this function as the value for the horizontal hoppings.  Verify that the example continues to run with unchanged results.

Hint: a hopping value function always takes two arguments, for example `site_i` and `site_j`, that correspond to the two sites of the hopping.  It may also take further parameters if needed.

```python
# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite

########
def hopx(...):
    return ...

model[sq(0, 0), sq(1, 0)] = ...
model[sq(0, 0), sq(0, 1)] = -1 / (2 * m * a**2)
########
```

```python
#### Solution of exercise D

# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite

########
def hopx(site_i, site_j):
    return -1 / (2 * m * a**2)

model[sq(0, 0), sq(1, 0)] = hopx
model[sq(0, 0), sq(0, 1)] = -1 / (2 * m * a**2)
########
```

```python
#### Verify that the result did not change.

fsyst = make_system(14, 9, model)
plot_conductance(fsyst, energy, 'voltage', np.linspace(0, -4, 50), params)
```

### Exercise E: Implement Peierls substitution
Add a parameter `B` to `hopx` and multiply the result with the phase `cmath.exp(-1j * B * y)` where `y` is the vertical component of the position of either `site_i` or `site_j` (both should be identical, you may verify that with `assert` if you like).  Note that you need to import `cmath`, the complex mathematics module of Python's standard library.  (You may use numpy instead, if you prefer.)

Execute the cells and verify that with zero magnetic field the results remain as before.

Now run the next cell where `B` is set to a non-zero value (`0.3` here) and observe how the plots change.

```python
# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite

########
def hopx(...):
    ...
########

model[sq(0, 0), sq(1, 0)] = hopx
model[sq(0, 0), sq(0, 1)] = -1 / (2 * m * a**2)
```

```python
#### Solution of exercise E

# Define value function (with three parameters) to be used in the model.
def onsite(site, voltage, width, thickness):
    x, y = site.pos
    return -voltage * exp(-(x / thickness)**2) * (1 - exp(-(y / width)**2))

# Create 2d model with two gates.
model = kwant.Builder(kwant.TranslationalSymmetry(
    sq.vec((1, 0)), sq.vec((0, 1))))
model[sq(0, 0)] = onsite

########
import cmath

def hopx(site_i, site_j, B):
        # The magnetic field is controlled by the parameter B.
        y = site_i.pos[1]
        assert y == site_j.pos[1]     # optional check
        return -1 / (2 * m * a**2) * cmath.exp(-1j * B * y)
########

model[sq(0, 0), sq(1, 0)] = hopx
model[sq(0, 0), sq(0, 1)] = -1 / (2 * m * a**2)
```

```python
#### Verify that the we still obtain the same result when B is set to zero.

fsyst = make_system(14, 9, model)
# We have to give a value to the new parameter, B, now.
params['B'] = 0
plot_conductance(fsyst, energy, 'voltage', np.linspace(0, -4, 50), params)
```

```python
#### Now set B to a different value and look at bands, conductance, and current density.
params['B'] = 0.3

kwant.plotter.bands(fsyst.leads[0], params=params)
plot_conductance(fsyst, energy, 'voltage', np.linspace(0, -4, 50), params)
plot_current_density(fsyst, energy, params)
```

### Exercise F: Experiment with parameter values
Our QPC is quite narrow.  You can make it wider (by increasing the value of the `W` parameter of `make_system` and the `width` parameter of the Hamiltonian) until you observe a clear edge state at one side of the sample.

```python
########
fsyst = make_system(14, 9, model)
params['B'] = 0.3
params['width'] = 3
########
kwant.plotter.map(fsyst, lambda i: fsyst.hamiltonian(i, i, params=params));
```

```python
#### Possible solution of exercise F

########
fsyst = make_system(14, 16, model)
params['B'] = 0.3
params['width'] = 7
########
kwant.plotter.map(fsyst, lambda i: fsyst.hamiltonian(i, i, params=params));
```

```python
kwant.plotter.bands(fsyst.leads[0], params=params)
plot_conductance(fsyst, energy, 'voltage', np.linspace(0, -4, 50), params)
plot_current_density(fsyst, energy, params)
```

```python

```
